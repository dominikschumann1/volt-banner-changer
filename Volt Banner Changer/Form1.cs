﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Volt_Banner_Changer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            completeLabel.Text = "";
            selectedLabel.Text = "";
            this.path = "";
        }

        private void loadPresetsFile()
        {
            savedPresets.Clear();
            String presetPath = path + "/presets.txt";
            if (File.Exists(presetPath))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(presetPath))
                    {
                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {
                            String[] values = line.Split(';');
                            savedPresets.Add(values[0], values[1]);
                        }
                        loadPresets();
                    }
                }
                catch (Exception err)
                {
                    // Let the user know what went wrong.
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(err.Message);
                    MessageBox.Show("Some thing went wrong wile reading: " + err.Message, "Something went wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        public void loadPresets()
        {
            this.presetComboBox.Items.Clear();

            foreach (var entry in savedPresets)
            {
                this.presetComboBox.Items.Add(entry.Key);
            }
        }

        String path;
        Dictionary<String, String> savedPresets = new Dictionary<string, string>();

        private void execute_Click(object sender, EventArgs e)
        {
            if (this.path == "")
            {
                MessageBox.Show("Please Select the volt-banner folder!", "Unvalid Paths", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String title = titleTextBox.Text;
            String subtitle = subTextBox.Text;

            if ((title == null || subtitle == null))
            {
                MessageBox.Show("All fields must be filled!", "Unvalid Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            List<string> html = new List<string>();

            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                using (StreamReader sr = new StreamReader(path + "/index.html"))
                {
                    string line;

                    // Read and display lines from the file until 
                    // the end of the file is reached. 
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains("${TITLE}"))
                        {
                            line = line.Replace("${TITLE}", title);
                        }
                        if (line.Contains("${SUBTITLE}"))
                        {
                            line = line.Replace("${SUBTITLE}", subtitle);
                        }
                        html.Add(line);
                    }
                }
            }
            catch (Exception err)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(err.Message);
                MessageBox.Show("Some thing went wrong wile reading: " + err.Message, "Something went wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            using (StreamWriter sw = new StreamWriter(path + "/output.html"))
            {
                foreach (string s in html)
                {
                    sw.WriteLine(s);
                }
            }

            completeLabel.Text = "Completed!";
        }

        private void titleTextBox_TextChanged(object sender, EventArgs e)
        {
            completeLabel.Text = "";
        }

        private void subTextBox_TextChanged(object sender, EventArgs e)
        {
            completeLabel.Text = "";
        }

        private void landComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            completeLabel.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    this.path = fbd.SelectedPath;
                    loadPresetsFile();
                    selectedLabel.Text = "Selected!";
                    this.presetComboBox.Enabled = true;
                    this.execute.Enabled = true;
                    this.button2.Enabled = true;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String presetsPath = this.path + "/presets.txt";
            if (this.savedPresets.ContainsKey(this.titleTextBox.Text))
            {
                MessageBox.Show("This Name was already saved as preset!", "Duplicate names", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            this.presetComboBox.Items.Add(this.titleTextBox.Text);
            String result = String.Format("{0};{1}\n", this.titleTextBox.Text, this.subTextBox.Text);

            savedPresets.Add(this.titleTextBox.Text, this.subTextBox.Text);

            if (!File.Exists(presetsPath))
            {
                FileStream presetFile = File.Create(presetsPath);
                presetFile.Close();
            }

            File.AppendAllText(presetsPath, result);
        }

        private void presetComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            String name = (String) this.presetComboBox.SelectedItem;
            string subtitle;

            if (savedPresets.TryGetValue(name, out subtitle))
            {
                this.titleTextBox.Text = name;
                this.subTextBox.Text = subtitle;
            }

        }
    }
}
